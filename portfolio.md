# ZXShade Portfolio

<figure style="position:relative;">
<img alt="Artistic shot of ZX81, spotlighting the ZX81 logo"
  src="image/ZX81-BarneyLivingston.jpg" />
<figcaption style="position:absolute; bottom:1rem; left:1rem; width:100%; color:#ddd"
  >CC-BY photo by BarneyLivingston</h2>
</figure>

The ZX81 was my first computer, an early 1980s micro with
extremely lo-fi display capabilities.
A black-and-white text-only 32×24 display; boosted very slightly by
having a set of
[semigraphic](https://en.wikipedia.org/wiki/Semigraphics) characters.

Can we make something interesting out of the ZX81 semigraphics?
(that we couldn’t do with some other system)

Each character of the ZX81 display is made on an 8×8 pixel grid.
Including the semigraphics; in ROM order they appear
like this:

![Display of the 22 semigraphic characters of the ZX81](image/zxsemi.png)

The bottom row is a reverse video of the top row.
This is one of the ZX81’s many space- and cost- saving tricks:
they didn’t have to be stored in ROM.

16 of the semigraphics are a 2×2 grid of blocks where
each block can be black or white.
Fine, but they are shared by many 1980s computers, so they
don’t model anything particular to the ZX81.
They could be used in a design, but it would be a design using
"generic 1980s semigraphics", not "ZX81 semigraphics".

The shaded (dithered) forms use a 1×2 arrangement of flat bricks, with
each of the two bricks being white, black, or dithered.
The bricks (8×4 pixels) have 2:1 aspect ratio.

The shading to me recalled designs like Umbra, Myopic (Chwast),
and in particular the South-shaded [Too
Much](https://fontsinuse.com/typefaces/44994/too-much).
Also Pioneer, which is easier to show an example of:

!["PIONEER" in the blocky angular and shaded font called Pioneer](image/pioneer.png)

I tried a design of **E** like

![E](image/E.png)

It’s simple enough, and the "flat bricks" (which are 8×4 pixels)
give a thick/thin contrast.

Each glyph in ZXShade is made from a small number of components
in a grid, where each component is one of the ZX81 semigraphics.
A small text file is used to describe each drawing.
For example, the description of **E** is:

    #II
    #I
    III

Note that in this case only 2 of the semigraphics are used (3
if you count /space).
Other glyphs use a few more of the semigraphics. Here’s **O**:

    CIC
    # #
    jIj

![O](image/O.png)

The entire font is constructed by program from the text
descriptions like the ones given above for **E** and **O**.
The letters (and /space and /numbersign) used in the text descriptions (above) come from
the font _Semi81_ which puts the semigraphics on those letters.
A shell script then uses `hb-view` to "print" the descriptions
to a PNG file.
Which is then converted to vector and a TTF by my own _Font 8_ tools.

The font _Semi81_ that is used in this process is also constructed
by program.

## Current font

![A block black-and-white alphabet with a south-shade. Closer
inspection shows that the shaded parts are in fact made from a
50% dither](zxshade-plaque-uc.svg)

![A block black-and-white set of numbers with a south-shade. Closer
inspection shows that the shaded parts are in fact made from a
50% dither](zxshade-plaque-numbers.svg)

Only the basic ASCII alphabet and numbers have been drawn.

## Revisions

It would be naive to think that this design is so simple that it
appeared in draft form with no changes required.
Here i have reconstructed from the git history some older
drawings.

![older, uglier, versions of J S Q Y](image/old-JSQY.svg)

I suspect there were quite a few versions that i tried while editing
that didn’t make it as far as git.

# END
