#!/bin/sh

set -e

# Create PNG files from glyph descriptions in glyph/

mkdir -p png

for a in glyph/*
do
    Base=$(basename "$a" .png)
    hb-view --background=000000 --foreground=ffffff --font-size 8 --margin=4 semi81.ttf "$(cat $a)" > png/"$Base".png
done

mkdir -p svg

vec8 -control control-zxshade.csv -scale 24:24 png/*.png > svg/zxshade.svg
ttf8 -dir zxshade -control control-zxshade.csv svg/zxshade.svg
f8name -dir zxshade -control control-zxshade.csv
assfont -dir zxshade

plak --output-format=svg --order code --lower zxshade.ttf |
  tee zxshade-plaque-uc.svg |
  rsvg-convert |
  kitty icat

hb-view --output-format=svg zxshade.ttf '01234
56789' |
  tee zxshade-plaque-numbers.svg |
  rsvg-convert |
  kitty icat
